Для компиляции.
Из src_ready:

gfortran -c mo_rte_kind.F90  mo_rte_util_array.F90 mo_gas_optics_rrtmgp_kernels.F90 mo_optical_props_kernels.F90 mo_fluxes_broadband_kernels.F90 mo_fluxes_byband_kernels.F90 mo_rte_solver_kernels.F90 FCWrapper.f90 rrtmgp.f90 head.f -ffree-line-length-none  -I/usr/include -L${netcdf_dir}/lib  -Wl,--start-group -lnetcdff -lnetcdf -lhdf5_hl  -lhdf5 -lz -Wl,--end-group

nvcc -c SubFunctions.cu Radiation_solver.cu Aerosol_optics.cu Cloud_optics.cu fluxes_kernels_launchers.cu Fluxes.cu Gas_concs.cu Gas_optics_rrtmgp.cu gas_optics_rrtmgp_kernels_launchers.cu mem_pool_gpu.cu Optical_props.cu optical_props_kernels_launchers.cu Rte_lw.cu rte_solver_kernels_launchers.cu Source_functions.cu subset_kernels_launchers.cu tools_gpu.cu -DUSECUDA

gcc -c SubFunctionsWrapper.c

nvcc -o main head.o rrtmgp.o FCWrapper.o SubFunctionsWrapper.o SubFunctions.o Radiation_solver.o Rte_lw.o Aerosol_optics.o Cloud_optics.o fluxes_kernels_launchers.o Fluxes.o Gas_concs.o gas_optics_rrtmgp_kernels_launchers.o Gas_optics_rrtmgp.o mem_pool_gpu.o mo_fluxes_broadband_kernels.o mo_fluxes_byband_kernels.o mo_gas_optics_rrtmgp_kernels.o mo_optical_props_kernels.o mo_rte_kind.o mo_rte_solver_kernels.o mo_rte_util_array.o optical_props_kernels_launchers.o Optical_props.o rte_solver_kernels_launchers.o Source_functions.o subset_kernels_launchers.o tools_gpu.o -lstdc++ -lgfortran -L${netcdf_dir}/lib   -lnetcdff -lnetcdf -DUSECUDA

Для запуска из run:
./../src_ready/main

Выводятся считанные данные из cpp-репозитория и реальной модели. Запускается инициализация и два раза вызывается вычисление радиации. Выводятся радиационные потоки для одного столбика.
При первом запуске cuda будет долго думать, вычисляя необходимые параметры и создавая *.txt файлы в run. Они ускоряют работу, удалять не надо.

Основные файлы для работы. 
- FCWrapper.f90 - основной файл оболочки.

- rrtmgp.f90 - из него вызывается lw-функция

- SubFunctions.cu - тут вызываемые функции инициализации и радиации, некоторые флаги. При изменении размерностей газов согласовать размерности в функциях read_and_set_vmr.

- SubFunctions.h - тут интерфейс этих функций

- SubFunctionsWrapper.c - при добавлении/удалении переменных согласовать переменные согласно интерфейсу функций (если с фортрана не переданы некоторые переменные поставить NULL)