#pragma once

typedef double Float;
#ifdef __cplusplus

extern "C" {
#endif

    void solve_radiation(int n_col_x, int n_col_y, int n_lay, int n_lev, Float* lay_p, Float* lay_t, Float* lev_p, Float* lev_t, 
                     Float* l_wp, Float* i_wp, Float* re_l, Float* re_i, Float* sfc_t, Float* sfc_emis, Float* dry_col,
                     Float* h2o, Float* co2, Float* o3, Float* n2o, Float* co, Float* ch4, Float* o2, Float* n2, Float* ccl4, Float* cfc11, Float* cfc12, 
                     Float* cfc22, Float* hfc143a, Float* hfc125, Float* hfc23, Float* hfc32, Float* hfc134a, Float* cf4, Float* no2,
                     Float* flux_up, Float* flux_dn, Float* flux_net);
                         
    void initialize();

#ifdef __cplusplus
}
#endif
