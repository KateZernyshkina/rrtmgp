/*
 * This file is a stand-alone executable developed for the
 * testing of the C++ interface to the RTE+RRTMGP radiation code.
 *
 * It is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/algorithm/string.hpp>
#include <chrono>
#include <iomanip>
#include <cuda_profiler_api.h>
#include "SubFunctions.h"


#include "Status.h"
#include "Netcdf_interface.h"
#include "Array.h"
#include "Radiation_solver.h"
#include "Gas_concs.h"
#include "Aerosol_optics.h"
#include "types.h"
#include "mem_pool_gpu.h"


void read_and_set_vmr(
        const std::string& gas_name, Float* gas, const int n_dims, const int n_col_x, const int n_col_y, const int n_lay, Gas_concs& gas_concs)
{
    if (gas)
    {
        if (n_dims == 0)
        {
           gas_concs.set_vmr(gas_name, *gas);
        }
        else if (n_dims == 1)
        {
           std::vector<Float> gas_vec;
           gas_vec.reserve(n_lay);
           gas_vec.assign(gas, gas + n_lay);
           gas_concs.set_vmr(gas_name, Array<Float,1>(gas_vec, {n_lay}));
        }
        else if (n_dims == 3)
        {
           std::vector<Float> gas_vec;
           gas_vec.reserve(n_col_x * n_col_y * n_lay);
           gas_vec.assign(gas, gas + n_col_x * n_col_y * n_lay);
           gas_concs.set_vmr(gas_name, Array<Float,2>(gas_vec, {n_col_x * n_col_y, n_lay}));
        }
    }
    else
    {
        /*Status::print_warning("Gas \"" + gas_name + "\" not available.");*/
    }
}

void initialize() {
    std::cout << "initialization\n";
    const Float val = 0.0;
    Gas_concs gas_concs;
    gas_concs.set_vmr(std::string{"h2o"}, val);
    gas_concs.set_vmr(std::string{"co2"}, val);
    gas_concs.set_vmr(std::string{"o3"}, val);
    gas_concs.set_vmr(std::string{"n2o"}, val);
    gas_concs.set_vmr(std::string{"ch4"}, val);
    Gas_concs_gpu gas_concs_gpu(gas_concs);
    Radiation_solver_longwave rad_lw(gas_concs_gpu, "coefficients_lw.nc", "cloud_coefficients_lw.nc");
}

/*void read_and_set_aer(
        const std::string& aerosol_name, const int n_col_x, const int n_col_y, const int n_lay,
        const Netcdf_handle& input_nc, Aerosol_concs& aerosol_concs)
{
    if (input_nc.variable_exists(aerosol_name))
    {
        std::map<std::string, int> dims = input_nc.get_variable_dimensions(aerosol_name);
        const int n_dims = dims.size();

        if (n_dims == 1)
        {
            if (dims.at("lay") == n_lay)
                aerosol_concs.set_vmr(aerosol_name,
                        Array<Float,1>(input_nc.get_variable<Float>(aerosol_name, {n_lay}), {n_lay}));
            else
                throw std::runtime_error("Illegal dimensions of gas \"" + aerosol_name + "\" in input");
        }
        else if (n_dims == 3)
        {
            if (dims.at("lay") == n_lay && dims.at("y") == n_col_y && dims.at("x") == n_col_x)
                aerosol_concs.set_vmr(aerosol_name,
                        Array<Float,2>(input_nc.get_variable<Float>(aerosol_name, {n_lay, n_col_y, n_col_x}), {n_col_x * n_col_y, n_lay}));
            else
                throw std::runtime_error("Illegal dimensions of \"" + aerosol_name + "\" in input");
        }
        else
            throw std::runtime_error("Illegal dimensions of \"" + aerosol_name + "\" in input");
    }
    else
    {
        throw std::runtime_error("Aerosol type \"" + aerosol_name + "\" not available in input file.");
    }
}*/


void configure_memory_pool(int nlays, int ncols, int nchunks, int ngpts, int nbnds)
{
    /* Heuristic way to set up memory pool queues */
    std::map<std::size_t, std::size_t> pool_queues = {
        {64, 20},
        {128, 20},
        {256, 10},
        {512, 10},
        {1024, 5},
        {2048, 5},
        {nchunks * ngpts * sizeof(Float), 16},
        {nchunks * nbnds * sizeof(Float), 16},
        {(nlays + 1) * ncols * sizeof(Float), 14},
        {(nlays + 1) * nchunks * sizeof(Float), 10},
        {(nlays + 1) * nchunks * nbnds * sizeof(Float), 4},
        {(nlays + 1) * nchunks * ngpts * sizeof(int)/2, 6},
        {(nlays + 1) * nchunks * ngpts * sizeof(Float), 18}
    };

    #ifdef GPU_MEM_POOL
    Memory_pool_gpu::init_instance(pool_queues);
    #endif
}

void print_command_line_options(
        const std::map<std::string, std::pair<bool, std::string>>& command_line_options)
{
    Status::print_message("Solver settings:");
    for (const auto& option : command_line_options)
    {
        std::ostringstream ss;
        ss << std::left << std::setw(20) << (option.first);
        ss << " = " << std::boolalpha << option.second.first << std::endl;
        Status::print_message(ss);
    }
}

extern "C" {
void solve_radiation(int n_col_x, int n_col_y, int n_lay, int n_lev, Float* lay_p, Float* lay_t, Float* lev_p, Float* lev_t, 
                     Float* l_wp, Float* i_wp, Float* re_l, Float* re_i, Float* sfc_t, Float* sfc_emis, Float* dry_col,
                     Float* h2o, Float* co2, Float* o3, Float* n2o, Float* co, Float* ch4, Float* o2, Float* n2, Float* ccl4, Float* cfc11, Float* cfc12, 
                     Float* cfc22, Float* hfc143a, Float* hfc125, Float* hfc23, Float* hfc32, Float* hfc134a, Float* cf4, Float* no2,
                     Float* flux_up, Float* flux_dn, Float* flux_net)
{
    Status::print_message("###### Starting RTE+RRTMGP solver ######");

    ////// FLOW CONTROL SWITCHES //////
    // Parse the command line options.
    std::map<std::string, std::pair<bool, std::string>> command_line_options {
        {"longwave"         , { true,  "Enable computation of longwave radiation." }},
        {"fluxes"           , { true,  "Enable computation of fluxes."             }},
        {"cloud-optics"     , { true,  "Enable cloud optics."                      }},
        {"aerosol-optics"   , { false, "Enable aerosol optics."                    }},
        {"output-optical"   , { false, "Enable output of optical."                 }},
        {"output-bnd-fluxes", { false, "Enable output of band fluxes."             }},
        {"timings"          , { false, "Repeat computation 10x for run times."     }}};

    const bool switch_longwave          = command_line_options.at("longwave"         ).first;
    const bool switch_fluxes            = command_line_options.at("fluxes"           ).first;
    const bool switch_cloud_optics      = command_line_options.at("cloud-optics"     ).first;
    const bool switch_aerosol_optics    = command_line_options.at("aerosol-optics"   ).first;
    const bool switch_output_optical    = command_line_options.at("output-optical"   ).first;
    const bool switch_output_bnd_fluxes = command_line_options.at("output-bnd-fluxes").first;
    const bool switch_timings           = command_line_options.at("timings"          ).first;
    
    // Print the options to the screen.
    //print_command_line_options(command_line_options);
    
    ////// READ THE ATMOSPHERIC DATA //////
    Status::print_message("Get atmospheric input data");

    //Netcdf_file input_nc("rte_rrtmgp_input.nc", Netcdf_mode::Read);

    const int n_col = n_col_x * n_col_y;
    /*std::cout << "x = " << n_col_x << std::endl;
    std::cout << "y = " << n_col_y << std::endl;
    std::cout << "lay = " << n_lay << std::endl;
    std::cout << "lev = " << n_lev << std::endl;
    std::cout << "n_col = " << n_col << std::endl;*/
    
    // Read the atmospheric fields.
    std::vector<Float> lay_vec_p;
    lay_vec_p.reserve(n_col * n_lay);
    lay_vec_p.assign(lay_p, lay_p + n_col * n_lay);
    Array<Float,2> p_lay(lay_vec_p, {n_col, n_lay});
    
    std::vector<Float> lay_vec_t;
    lay_vec_t.reserve(n_col * n_lay);
    lay_vec_t.assign(lay_t, lay_t + n_col * n_lay);
    Array<Float,2> t_lay(lay_vec_t, {n_col, n_lay});
    
    std::vector<Float> lev_vec_p;
    lev_vec_p.reserve(n_col * n_lev);
    lev_vec_p.assign(lev_p, lev_p + n_col * n_lev);
    Array<Float,2> p_lev(lev_vec_p, {n_col, n_lev});
    
    std::vector<Float> lev_vec_t;
    lev_vec_t.reserve(n_col * n_lev);
    lev_vec_t.assign(lev_t, lev_t + n_col * n_lev);
    Array<Float,2> t_lev(lev_vec_t, {n_col, n_lev});
    
    Array<Float,2> col_dry;
    std::vector<Float> col_dry_vec;
    if (dry_col) {
        col_dry.set_dims({n_col, n_lay});
        col_dry_vec.reserve(n_col * n_lay);
        col_dry_vec.assign(dry_col, dry_col + n_col * n_lay);
        col_dry = std::move(col_dry_vec);
    }

    // Create container for the gas concentrations and read gases.
    Gas_concs gas_concs;

    read_and_set_vmr("h2o", h2o, 3, n_col_x, n_col_y, n_lay, gas_concs);      // third arg is number of dimensions
    read_and_set_vmr("co2", co2, 3, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("o3" , o3 , 3, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("n2o", n2o, 3, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("co" , co , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("ch4", ch4, 3, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("o2" , o2 , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("n2" , n2 , 0, n_col_x, n_col_y, n_lay, gas_concs);

    read_and_set_vmr("ccl4"   , ccl4   , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("cfc11"  , cfc11  , 3, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("cfc12"  , cfc12  , 3, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("cfc22"  , cfc22  , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("hfc143a", hfc143a, 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("hfc125" , hfc125 , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("hfc23"  , hfc23  , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("hfc32"  , hfc32  , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("hfc134a", hfc134a, 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("cf4"    , cf4    , 0, n_col_x, n_col_y, n_lay, gas_concs);
    read_and_set_vmr("no2"    , no2    , 0, n_col_x, n_col_y, n_lay, gas_concs);

    Array<Float,2> lwp;
    Array<Float,2> iwp;
    Array<Float,2> rel;
    Array<Float,2> rei;
    std::vector<Float> lwp_vec;
    std::vector<Float> iwp_vec;
    std::vector<Float> rel_vec;
    std::vector<Float> rei_vec;

    if (switch_cloud_optics)
    {
        lwp.set_dims({n_col, n_lay});
        lwp_vec.reserve(n_col * n_lay);
        lwp_vec.assign(l_wp, l_wp + n_col * n_lay);
        lwp = std::move(lwp_vec);

        iwp.set_dims({n_col, n_lay});
        iwp_vec.reserve(n_col * n_lay);
        iwp_vec.assign(i_wp, i_wp + n_col * n_lay);
        iwp = std::move(iwp_vec);

        rel.set_dims({n_col, n_lay});
        rel_vec.reserve(n_col * n_lay);
        rel_vec.assign(re_l, re_l + n_col * n_lay);
        rel = std::move(rel_vec);

        rei.set_dims({n_col, n_lay});
        rei_vec.reserve(n_col * n_lay);
        rei_vec.assign(re_i, re_i + n_col * n_lay);
        rei = std::move(rei_vec);
    }

    /*Array<Float,2> rh;                     // will be later?
    std::vector<Float> rh_vec;
    Aerosol_concs aerosol_concs;

    if (switch_aerosol_optics)
    {
        rh.set_dims({n_col, n_lay});
        rh_vec.reserve(n_col * n_lay);
        rh_vec.assign(rh_value, rh_value + n_col * n_lay);
        rh = std::move(rh_vec);

        read_and_set_aer("aermr01", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr02", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr03", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr04", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr05", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr06", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr07", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr08", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr09", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr10", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
        read_and_set_aer("aermr11", n_col_x, n_col_y, n_lay, input_nc, aerosol_concs);
    }*/



    ////// CREATE THE OUTPUT FILE //////
    // Create the general dimensions and arrays.
    //Status::print_message("Preparing NetCDF output file.");

    int ngpts = 0;
    int nbnds = 0;
    if (switch_longwave)
    {
        /*Netcdf_file coef_nc_lw("coefficients_lw.nc", Netcdf_mode::Read);
        nbnds = std::max(coef_nc_lw.get_dimension_size("bnd"), nbnds);
        ngpts = std::max(coef_nc_lw.get_dimension_size("gpt"), ngpts);*/
        nbnds = 16;    // from coefficients_lw.nc as shown above
        ngpts = 256;
    }
    configure_memory_pool(n_lay, n_col, 512, ngpts, nbnds);


    ////// RUN THE LONGWAVE SOLVER //////
    if (switch_longwave)
    {
        // Initialize the solver.
        Status::print_message("Initializing the longwave solver.");

        Gas_concs_gpu gas_concs_gpu(gas_concs);

        Radiation_solver_longwave rad_lw(gas_concs_gpu, "default", "default"); 

        // Read the boundary conditions.
        const int n_bnd_lw = rad_lw.get_n_bnd_gpu();
        const int n_gpt_lw = rad_lw.get_n_gpt_gpu();

        /*std::vector<Float> emis_sfc_vec;      // if sfc_emis is 3d-array (ncol * n_bnd_lw)
        emis_sfc_vec.reserve(n_col * n_bnd_lw);
        emis_sfc_vec.assign(sfc_emis, sfc_emis + n_col * n_bnd_lw);
        Array<Float,2> emis_sfc(emis_sfc_vec, {n_bnd_lw, n_col});*/
        
        std::vector<Float> emis_sfc_vec; //************************* not very good (cloning the data)
        emis_sfc_vec.reserve(n_col * n_bnd_lw);
        for (int i = 0; i < n_bnd_lw; i++) {
            for (int j = 0; j < n_col; j++) {
                emis_sfc_vec[i * n_col + j] = sfc_emis[j];
                //std::cout << emis_sfc_vec[i * n_col + j] << " ";
            }
        }
        Array<Float,2> emis_sfc(emis_sfc_vec, {n_bnd_lw, n_col});
        
        std::vector<Float> t_sfc_vec;
        t_sfc_vec.reserve(n_col);
        t_sfc_vec.assign(sfc_t, sfc_t + n_col);
        Array<Float,1> t_sfc(t_sfc_vec, {n_col});

        // Create output arrays.
        Array_gpu<Float,3> lw_tau;
        Array_gpu<Float,3> lay_source;
        Array_gpu<Float,3> lev_source_inc;
        Array_gpu<Float,3> lev_source_dec;
        Array_gpu<Float,2> sfc_source;

        if (switch_output_optical)
        {
            lw_tau        .set_dims({n_col, n_lay, n_gpt_lw});
            lay_source    .set_dims({n_col, n_lay, n_gpt_lw});
            lev_source_inc.set_dims({n_col, n_lay, n_gpt_lw});
            lev_source_dec.set_dims({n_col, n_lay, n_gpt_lw});
            sfc_source    .set_dims({n_col, n_gpt_lw});
        }

        Array_gpu<Float,2> lw_flux_up;
        Array_gpu<Float,2> lw_flux_dn;
        Array_gpu<Float,2> lw_flux_net;

        if (switch_fluxes)
        {
            lw_flux_up .set_dims({n_col, n_lev});
            lw_flux_dn .set_dims({n_col, n_lev});
            lw_flux_net.set_dims({n_col, n_lev});
        }

        Array_gpu<Float,3> lw_bnd_flux_up;
        Array_gpu<Float,3> lw_bnd_flux_dn;
        Array_gpu<Float,3> lw_bnd_flux_net;

        if (switch_output_bnd_fluxes)
        {
            lw_bnd_flux_up .set_dims({n_col, n_lev, n_bnd_lw});
            lw_bnd_flux_dn .set_dims({n_col, n_lev, n_bnd_lw});
            lw_bnd_flux_net.set_dims({n_col, n_lev, n_bnd_lw});
        }


        // Solve the radiation.

        Status::print_message("Solving the longwave radiation.");

        auto run_solver = [&]()
        {
            Array_gpu<Float,2> p_lay_gpu(p_lay);
            Array_gpu<Float,2> p_lev_gpu(p_lev);
            Array_gpu<Float,2> t_lay_gpu(t_lay);
            Array_gpu<Float,2> t_lev_gpu(t_lev);
            Array_gpu<Float,2> col_dry_gpu(col_dry);
            Array_gpu<Float,1> t_sfc_gpu(t_sfc);
            Array_gpu<Float,2> emis_sfc_gpu(emis_sfc);
            Array_gpu<Float,2> lwp_gpu(lwp);
            Array_gpu<Float,2> iwp_gpu(iwp);
            Array_gpu<Float,2> rel_gpu(rel);
            Array_gpu<Float,2> rei_gpu(rei);

            cudaDeviceSynchronize();
            cudaEvent_t start;
            cudaEvent_t stop;
            cudaEventCreate(&start);
            cudaEventCreate(&stop);

            cudaEventRecord(start, 0);

            rad_lw.solve_gpu(
                    switch_fluxes,
                    switch_cloud_optics,
                    switch_output_optical,
                    switch_output_bnd_fluxes,
                    gas_concs_gpu,
                    p_lay_gpu, p_lev_gpu,
                    t_lay_gpu, t_lev_gpu,
                    col_dry_gpu,
                    t_sfc_gpu, emis_sfc_gpu,
                    lwp_gpu, iwp_gpu,
                    rel_gpu, rei_gpu,
                    lw_tau, lay_source, lev_source_inc, lev_source_dec, sfc_source,
                    lw_flux_up, lw_flux_dn, lw_flux_net,
                    lw_bnd_flux_up, lw_bnd_flux_dn, lw_bnd_flux_net);

            cudaEventRecord(stop, 0);
            cudaEventSynchronize(stop);
            float duration = 0.f;
            cudaEventElapsedTime(&duration, start, stop);

            cudaEventDestroy(start);
            cudaEventDestroy(stop);

            Status::print_message("Duration longwave solver: " + std::to_string(duration) + " (ms)");
        };

        // Tuning step;
        // run_solver();

        // Profiling step;
        cudaProfilerStart();
        run_solver();
        cudaProfilerStop();

        if (switch_timings)
        {
            constexpr int n_measures=10;
            for (int n=0; n<n_measures; ++n)
                run_solver();
        }

        //// Store the output.
        Status::print_message("Storing the longwave output.");
        Array<Float,2> lw_flux_up_cpu(lw_flux_up);
        Array<Float,2> lw_flux_dn_cpu(lw_flux_dn);
        Array<Float,2> lw_flux_net_cpu(lw_flux_net);
        /*Array<Float,3> lw_bnd_flux_up_cpu(lw_bnd_flux_up);
        Array<Float,3> lw_bnd_flux_dn_cpu(lw_bnd_flux_dn);
        Array<Float,3> lw_bnd_flux_net_cpu(lw_bnd_flux_net);*/

        if (switch_fluxes)
        {
            Float* flux_up_ptr = lw_flux_up_cpu.ptr();
            Float* flux_dn_ptr = lw_flux_dn_cpu.ptr();
            Float* flux_net_ptr = lw_flux_net_cpu.ptr();
            
            for (int i = 0; i < n_col * n_lev; i++) {
                flux_up[i] = flux_up_ptr[i];
                flux_dn[i] = flux_dn_ptr[i];
                flux_net[i] = flux_net_ptr[i];
            }

            /*if (switch_output_bnd_fluxes)                          // before using add args to func: bnd_flux_up, bnd_flux_dn, bnd_flux_net
            {
                Float* bnd_flux_up_ptr = lw_bnd_flux_up_cpu.ptr();
                Float* bnd_flux_dn_ptr = lw_bnd_flux_dn_cpu.ptr();
                Float* bnd_flux_net_ptr = lw_bnd_flux_net_cpu.ptr();
            
                for (int i = 0; i < n_col * n_lev; i++) {
                    bnd_flux_up[i] = bnd_flux_up_ptr[i];
                    bnd_flux_dn[i] = bnd_flux_dn_ptr[i];
                    bnd_flux_net[i] = bnd_flux_net_ptr[i];
                }
            }*/
        }
    }
    Status::print_message("###### Finished RTE+RRTMGP solver ######");
}
}
