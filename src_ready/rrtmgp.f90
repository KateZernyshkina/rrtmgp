

      Module mod_rrtmgp
        Use netcdf
        USE LWModule
        
        Implicit None
        
        Real(Kind=8) :: PI
        
      CONTAINS
        
        Subroutine rrtmgp()
          Implicit None
          Integer(Kind=4) ncid, did, varid, x, y, lay, lev, band_lw, latm, column, aer, level, i
          Character(300) in_fn, varname
          Logical stat
          Real(Kind=8), Allocatable :: lay_p(:, :, :), lay_t(:, :, :), lev_p(:, :, :), lev_t(:, :, :), &
          & l_wp(:, :, :), i_wp(:, :, :), re_l(:, :, :), re_i(:, :, :), sfc_t(:, :), sfc_emis(:, :, :), &
          & h2o(:, :, :), o3(:, :, :), flux_up_ex(:, :, :), flux_dn_ex(:, :, :), flux_net_ex(:, :, :), &
          & cfc11_vmr(:, :, :), cfc12_vmr(:, :, :), ch4_vmr(:, :, :), co2_vmr(:, :, :), n2o_vmr(:, :, :), &
          & o3_mmr(:, :, :), h2o_vmr(:, :, :), pressure_hl(:, :, :), q_ice(:, :, :), q_liquid(:, :, :), re_ice(:, :, :), &
          & re_liquid(:, :, :), skin_temperature(:, :), temperature_hl(:, :, :), lw_emissivity(:, :), &
          & aerosol_mmr(:, :, :, :), cloud_fraction(:, :, :), temp_lay(:, :, :), temp_lev(:, :, :), &
          pres_lay(:, :, :), pres_lev(:, :, :), flux_up(:, :, :), flux_dn(:, :, :), flux_net(:, :, :)
          Real(Kind=8) :: co2, ch4, n2o, n2, o2, o2_vmr
          
          in_fn = "rte_rrtmgp_input.nc"
          
          call check( "Open file "//trim(in_fn), nf90_open (path = trim(in_fn), mode = IOR(NF90_NOWRITE,NF90_NETCDF4), ncid = ncid) )
          
          varname = "x"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get x"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = x  ) )
          
          varname = "y"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get y"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = y  ) )
          
          varname = "lay"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get lay"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = lay  ) )
          
          varname = "lev"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get lev"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = lev  ) )
          
          varname = "band_lw"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get band_lw"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = band_lw  ) )
          
          
          Allocate(flux_up_ex(x, y, lev)); flux_up_ex = 0.
          Allocate(flux_dn_ex(x, y, lev)); flux_dn_ex = 0.
          Allocate(flux_net_ex(x, y, lev)); flux_net_ex = 0.
          
          Allocate(lay_p(x, y, lay)); lay_p = 0.
          varname = "p_lay"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, lay_p, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          !write(*,'(24(f15.7))') lay_p
          
          Allocate(lay_t(x, y, lay)); lay_t = 0.
          varname = "t_lay"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, lay_t, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          Allocate(lev_p(x, y, lev)); lev_p = 0.
          varname = "p_lev"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, lev_p, (/ 1, 1, 1 /), (/ x, y, lev /)) )
          !write(*,'(24(f15.7))') lev_p
          
          Allocate(lev_t(x, y, lev)); lev_t = 0.
          varname = "t_lev"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, lev_t, (/ 1, 1, 1 /), (/ x, y, lev /)) )
          
          Allocate(l_wp(x, y, lay)); l_wp = 0.
          varname = "lwp"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, l_wp, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          Allocate(i_wp(x, y, lay)); i_wp = 0.
          varname = "iwp"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, i_wp, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          Allocate(re_l(x, y, lay)); re_l = 0.
          varname = "rel"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, re_l, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          Allocate(re_i(x, y, lay)); re_i = 0.
          varname = "rei"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, re_i, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          Allocate(sfc_t(x, y)); sfc_t = 0.
          varname = "t_sfc"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, sfc_t, (/ 1, 1 /), (/ x, y /)) )
          
          Allocate(sfc_emis(band_lw, x, y)); sfc_emis = 0.
          varname = "emis_sfc"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, sfc_emis, (/ 1, 1, 1 /), (/ band_lw, x, y /)) )
          
          Allocate(h2o(x, y, lay)); h2o = 0.
          varname = "vmr_h2o"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, h2o, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          Allocate(o3(x, y, lay)); o3 = 0.
          varname = "vmr_o3"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, o3, (/ 1, 1, 1 /), (/ x, y, lay /)) )
          
          varname = "vmr_co2"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, co2) )
          
          varname = "vmr_ch4"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, ch4) )
          
          varname = "vmr_n2o"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, n2o) )
          
          varname = "vmr_n2"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, n2) )
          
          varname = "vmr_o2"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, o2) )
          
          call check("Close file ", nf90_close (ncid))
          
          !=======================================================================================
          
          in_fn = "zz_diag_eb-latm.nc"
          
          call check( "Open file "//trim(in_fn), nf90_open (path = trim(in_fn), mode = IOR(NF90_NOWRITE,NF90_NETCDF4), ncid = ncid) )
          
          varname = "column"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get column"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = column  ) )
          
          varname = "latm"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get latm"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = latm  ) )
          
          varname = "level"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get level"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = level  ) )
          
          varname = "aer"
          call check( "looking for "//trim(varname), nf90_inq_dimid        (ncid = ncid, name = trim(varname), dimid = did) )
          call check( "get aer"        , nf90_inquire_dimension(ncid = ncid, dimid = did , len = aer  ) )
          
          print *,"latm = ",latm
          print *,"column = ",column
          print *,"aer = ",aer
          print *,"level = ",level
          
          Allocate(flux_up(latm, column, level + 1)); flux_up = 0.
          Allocate(flux_dn(latm, column, level + 1)); flux_dn = 0.
          Allocate(flux_net(latm, column, level + 1)); flux_net = 0.
          
          Allocate(aerosol_mmr(level, aer, column, latm)); aerosol_mmr = 0.
          Allocate(cfc11_vmr(level, column, latm));        cfc11_vmr = 0.
          Allocate(cfc12_vmr(level, column, latm));        cfc12_vmr = 0.
          Allocate(ch4_vmr(level, column, latm));          ch4_vmr = 0.
          Allocate(cloud_fraction(level, column, latm));   cloud_fraction = 0.
          Allocate(co2_vmr(level, column, latm));          co2_vmr = 0.
          Allocate(lw_emissivity(column, latm));           lw_emissivity = 0.
          Allocate(n2o_vmr(level, column, latm));          n2o_vmr = 0.
          Allocate(o3_mmr(level, column, latm));           o3_mmr = 0.
          Allocate(pressure_hl(level, column, latm));      pressure_hl = 0.
          Allocate(q_ice(level, column, latm));            q_ice = 0.
          Allocate(q_liquid(level, column, latm));         q_liquid = 0.
          Allocate(re_ice(level, column, latm));           re_ice = 0.
          Allocate(re_liquid(level, column, latm));        re_liquid = 0.
          Allocate(skin_temperature(column, latm));        skin_temperature = 0.
          Allocate(temperature_hl(level, column, latm));   temperature_hl = 0.
          Allocate(temp_lay(level, column, latm));         temp_lay = 0.
          Allocate(temp_lev(level + 1, column, latm));     temp_lev = 0.
          Allocate(pres_lay(level, column, latm));         pres_lay = 0.
          Allocate(pres_lev(level + 1, column, latm));     pres_lev = 0.
          Allocate(h2o_vmr(level, column, latm));         h2o_vmr = 0.017
          
          do i = 1, level - 1
              h2o_vmr(level - i, :, :) = 0.88 * h2o_vmr(level - i + 1, 1, 1)
          end do
          
          write(*, *) 'h2o_vmr'
          write(*,'(400(f12.4))') h2o_vmr(:,1,1)
          write(*, *) 'h2o'
          write(*,'(400(f12.4))') h2o(1,1,:)
          
          varname = "aerosol_mmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, aerosol_mmr, &
          & (/ 1, 1, 1, 1 /), (/ level, aer, column, latm /)) )
          
          varname = "o2_vmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, o2_vmr) )
          
          print *,"o2_vmr = ", o2_vmr
          print *,"o2 = ", o2
          
          varname = "cfc11_vmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, cfc11_vmr, (/ 1, 1, 1 /), (/ level, column, latm /)) )
          
          write(*, *) 'cfc11_vmr'
          write(*,'(400(f12.4))') cfc11_vmr(:,1,1)
          
          varname = "cfc12_vmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, cfc12_vmr, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'cfc12_vmr'
          write(*,'(400(f12.4))') cfc12_vmr(:,1,1)
          
          varname = "ch4_vmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, ch4_vmr, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'ch4_vmr'
          write(*,'(400(f12.4))') ch4_vmr(:,1,1)
          print *,"ch4 = ", ch4
          
          varname = "cloud_fraction"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, cloud_fraction, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          varname = "co2_vmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, co2_vmr, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'co2_vmr'
          write(*,'(400(f12.4))') co2_vmr(:,1,1)
          print *,"co2 = ", co2
          
          varname = "n2o_vmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, n2o_vmr, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'n2o_vmr'
          write(*,'(400(f12.4))') n2o_vmr(:,1,1)
          print *,"n2o = ", n2o
          
          varname = "o3_mmr"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, o3_mmr, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'o3_mmr'
          write(*,'(400(f12.4))') o3_mmr(:,1,1)
          write(*, *) 'o3'
          write(*,'(400(f12.4))') o3(1,1,:)
          
          varname = "pressure_hl"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, pressure_hl, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          varname = "re_ice"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, re_ice, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 're_ice'
          write(*,'(400(f12.4))') re_ice(:,1,1)
          write(*, *) 're_i'
          write(*,'(400(f12.4))') re_i(1,1,:)
          
          varname = "re_liquid"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, re_liquid, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 're_liquid'
          write(*,'(400(f12.4))') re_liquid(:,1,1)
          write(*, *) 're_l'
          write(*,'(400(f12.4))') re_l(1,1,:)
          
          varname = "temperature_hl"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, temperature_hl, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          varname = "skin_temperature"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, skin_temperature, (/ 1, 1 /),  (/ column, latm /)) )
          
          print *,"skin_temperature = ", skin_temperature(1,1)
          print *,"t_sfc = ", sfc_t(1,1)
          
          
          varname = "q_ice"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, q_ice, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'q_ice'
          write(*,'(400(f12.4))') q_ice(:,1,1)
          write(*, *) 'i_wp'
          write(*,'(400(f12.4))') i_wp(1,1,:)
          
          varname = "q_liquid"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, q_liquid, (/ 1, 1, 1 /),  (/ level, column, latm /)) )
          
          write(*, *) 'q_liquid'
          write(*,'(400(f12.4))') q_liquid(:,1,1)
          write(*, *) 'l_wp'
          write(*,'(400(f12.4))') l_wp(1,1,:)
          
          varname = "lw_emissivity"
          call check( "looking for "//trim(varname), nf90_inq_varid (ncid, trim(varname), varid))
          call check( "get "        //trim(varname), nf90_get_var   (ncid, varid, lw_emissivity, (/ 1, 1 /),  (/ column, latm /)) )
          
          print *,"lw_emissivity = ", lw_emissivity(1,1)
          write(*, *) 'sfc_emis'
          write(*,'(400(f12.4))') sfc_emis(1,1,:)
          
          
          temp_lev(2:level+1,:,:) = temperature_hl
          temp_lev(1,:,:) = 2 * temperature_hl(1,:,:) - temperature_hl(2,:,:)
          !write(*,'(400(f12.3))') reshape(temp_lev, [latm, column, level + 1], order = [3, 2, 1])
          
          write(*, *) 'temp_lev'
          write(*,'(400(f12.4))') temp_lev(:,1,1)
          write(*, *) 'lev_t'
          write(*,'(400(f12.4))') lev_t(1,1,:)
          
          pres_lev(2:level+1,:,:) = pressure_hl
          pres_lev(1,:,:) = 2 * pressure_hl(1,:,:) - pressure_hl(2,:,:)
          !write(*,'(400(f12.3))') reshape(pres_lev, [latm, column, level + 1], order = [3, 2, 1])
          
          write(*, *) 'pres_lev'
          write(*,'(400(f12.4))') pres_lev(:,1,1)
          write(*, *) 'lev_p'
          write(*,'(400(f12.4))') lev_p(1,1,:)
          
          temp_lay = (temp_lev(1:level,:,:) + temp_lev(2:level+1,:,:)) / 2
          
          write(*, *) 'temp_lay'
          write(*,'(400(f12.4))') temp_lay(:,1,1)
          write(*, *) 'lay_t'
          write(*,'(400(f12.4))') lay_t(1,1,:)
          
          pres_lay = (pres_lev(1:level,:,:) + pres_lev(2:level+1,:,:)) / 2
          !write(*,'(400(f12.3))') reshape(pres_lay, [latm, column, level], order = [3, 2, 1])
          
          write(*, *) 'pres_lay'
          write(*,'(400(f12.4))') pres_lay(:,1,1)
          write(*, *) 'lay_p'
          write(*,'(400(f12.4))') lay_p(1,1,:)
          
          call check("Close file ", nf90_close (ncid))
          
          !call lw_rrtmgp(x, y, lay, lev, lay_p, lay_t, lev_p, lev_t, band_lw, l_wp, i_wp, re_l, re_i, sfc_t, sfc_emis, &
          !            & h2o, co2, o3, n2o, ch4, o2, n2, flux_up_ex, flux_dn_ex, flux_net_ex)
          call lw_init()
          
          lev = level + 1
          call lw_rrtmgp(latm, column, level, lev, reshape(pres_lay, [latm, column, level], order = [3, 2, 1]), &
                                                  & reshape(temp_lay, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(pres_lev, [latm, column, level + 1], order = [3, 2, 1]),  &
                                                  & reshape(temp_lev, [latm, column, level + 1], order = [3, 2, 1]),  &
                                                  & reshape(q_liquid, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(q_ice, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(re_liquid, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(re_ice, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(skin_temperature, [latm, column], order = [2, 1]),  &
                                                  & reshape(lw_emissivity, [latm, column], order = [2, 1]),  &
                                                  & reshape(h2o_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(co2_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(o3_mmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(n2o_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(ch4_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & o2_vmr, &
                                                  & reshape(cfc11_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(cfc12_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & flux_up, flux_dn, flux_net)
                                                  
          call lw_rrtmgp(latm, column, level, lev, reshape(pres_lay, [latm, column, level], order = [3, 2, 1]), &
                                                  & reshape(temp_lay, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(pres_lev, [latm, column, level + 1], order = [3, 2, 1]),  &
                                                  & reshape(temp_lev, [latm, column, level + 1], order = [3, 2, 1]),  &
                                                  & reshape(q_liquid, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(q_ice, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(re_liquid, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(re_ice, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(skin_temperature, [latm, column], order = [2, 1]),  &
                                                  & reshape(lw_emissivity, [latm, column], order = [2, 1]),  &
                                                  & reshape(h2o_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(co2_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(o3_mmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(n2o_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(ch4_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & o2_vmr, &
                                                  & reshape(cfc11_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & reshape(cfc12_vmr, [latm, column, level], order = [3, 2, 1]),  &
                                                  & flux_up, flux_dn, flux_net)
          
          write(*, *) 'flux_up'
          write(*,'(400(f12.4))') flux_up(1,1,:)
          write(*, *) 'flux_dn'
          write(*,'(400(f12.4))') flux_dn(1,1,:)
          write(*, *) 'flux_net'
          write(*,'(400(f12.4))') flux_net(1,1,:)
          
          if (Allocated(lay_p)) Deallocate(lay_p)
          if (Allocated(lay_t)) Deallocate(lay_t)
          if (Allocated(lev_p)) Deallocate(lev_p)
          if (Allocated(lev_t)) Deallocate(lev_t)
          if (Allocated(l_wp)) Deallocate(l_wp)
          if (Allocated(i_wp)) Deallocate(i_wp)
          if (Allocated(re_l)) Deallocate(re_l)
          if (Allocated(re_i)) Deallocate(re_i)
          if (Allocated(sfc_t)) Deallocate(sfc_t)
          if (Allocated(sfc_emis)) Deallocate(sfc_emis)
          if (Allocated(h2o)) Deallocate(h2o)
          if (Allocated(o3)) Deallocate(o3)
          if (Allocated(flux_up)) Deallocate(flux_up)
          if (Allocated(flux_dn)) Deallocate(flux_dn)
          if (Allocated(flux_net)) Deallocate(flux_net)
          
          if (Allocated(aerosol_mmr)) Deallocate(aerosol_mmr)
          if (Allocated(cfc11_vmr)) Deallocate(cfc11_vmr)
          if (Allocated(cfc12_vmr)) Deallocate(cfc12_vmr)
          if (Allocated(ch4_vmr)) Deallocate(ch4_vmr)
          if (Allocated(cloud_fraction)) Deallocate(cloud_fraction)
          if (Allocated(co2_vmr)) Deallocate(co2_vmr)
          if (Allocated(lw_emissivity)) Deallocate(lw_emissivity)
          if (Allocated(n2o_vmr)) Deallocate(n2o_vmr)
          if (Allocated(o3_mmr)) Deallocate(o3_mmr)
          if (Allocated(pressure_hl)) Deallocate(pressure_hl)
          if (Allocated(q_ice)) Deallocate(q_ice)
          if (Allocated(q_liquid)) Deallocate(q_liquid)
          if (Allocated(re_ice)) Deallocate(re_ice)
          if (Allocated(re_liquid)) Deallocate(re_liquid)
          if (Allocated(skin_temperature)) Deallocate(skin_temperature)
          if (Allocated(temperature_hl)) Deallocate(temperature_hl)
          
        End Subroutine
        
        Subroutine check(code, stat)
          Implicit None
          Character(*),    Intent(In) :: code
          Integer(Kind=4), Intent(In) :: stat
          if (stat /= NF90_NOERR) then
            write(*,"(3x,2x,a,a,2x,a,2x,a)") "var (code): ",code,"mess: ",trim(NF90_STRERROR(stat))
            STOP
          end if
        End Subroutine
        
      End Module




