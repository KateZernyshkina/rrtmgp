#include <stdlib.h>
#include <stdio.h>
#include "SubFunctions.h"

/* example
void solve_radiation(int n_col_x, int n_col_y, int n_lay, int n_lev, float* lay_p, float* lay_t, float* lev_p, float* lev_t, 
                     float* l_wp, float* i_wp, float* re_l, float* re_i, float* sfc_t, float* sfc_emis, float* dry_col,
                     float* h2o, float* co2, float* o3, float* n2o, float* co, float* ch4, float* o2, float* n2, float* ccl4, float* cfc11, float* cfc12, 
                     float* cfc22, float* hfc143a, float* hfc125, float* hfc23, float* hfc32, float* hfc134a, float* cf4, float* no2,
                     float* flux_up, float* flux_dn, float* flux_net) */


void lw_func(int* x, int* y, int* lay, int* lev, Float* lay_p, Float* lay_t, Float* lev_p, Float* lev_t, 
             Float* l_wp, Float* i_wp, Float* re_l, Float* re_i, Float* sfc_t, Float* sfc_emis, 
             Float* h2o, Float* co2, Float* o3, Float* n2o, Float* ch4, Float* o2, Float* cfc11, Float* cfc12, 
             Float* flux_up, Float* flux_dn, Float* flux_net) {
    solve_radiation(*x, *y, *lay, *lev, lay_p, lay_t, lev_p, lev_t, 
                    l_wp, i_wp, re_l, re_i, sfc_t, sfc_emis, NULL,
                    h2o, co2, o3, n2o, NULL, ch4, o2, NULL, NULL, cfc11, cfc12,
                    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
                    flux_up, flux_dn, flux_net);
}

void init() {
    initialize();
}

