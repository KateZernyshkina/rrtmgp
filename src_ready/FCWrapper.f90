module C_FUNC
    INTERFACE
    
        SUBROUTINE lw_func(x, y, lay, lev, lay_p, lay_t, lev_p, lev_t, l_wp, i_wp, re_l, re_i, sfc_t, sfc_emis, &
                     & h2o, co2, o3, n2o, ch4, o2, cfc11, cfc12, flux_up, flux_dn, flux_net) BIND(C)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_DOUBLE
            IMPLICIT NONE
            INTEGER(C_INT) :: x, y, lay, lev
            REAL(C_DOUBLE), DIMENSION(x, y, lay) :: lay_p, lay_t, l_wp, i_wp, re_l, re_i, o3, h2o, co2, n2o, ch4, cfc11, cfc12
            REAL(C_DOUBLE), DIMENSION(x, y, lev) :: lev_p, lev_t, flux_up, flux_dn, flux_net
            REAL(C_DOUBLE), DIMENSION(x, y) :: sfc_t, sfc_emis
            REAL(C_DOUBLE) :: o2
        END SUBROUTINE lw_func
        
        SUBROUTINE init() BIND(C)
        END SUBROUTINE init
        
    END INTERFACE
end module C_FUNC

MODULE LWModule
    CONTAINS
    
    SUBROUTINE lw_rrtmgp(x, y, lay, lev, lay_p, lay_t, lev_p, lev_t, l_wp, i_wp, re_l, re_i, sfc_t, sfc_emis, &
                       & h2o, co2, o3, n2o, ch4, o2, cfc11, cfc12, flux_up, flux_dn, flux_net)
        USE C_FUNC
        IMPLICIT NONE
        INTEGER :: x, y, lay, lev
        REAL(Kind=8), DIMENSION(:, :, :) :: lay_p, lay_t, lev_p, lev_t, l_wp, i_wp, re_l, re_i, o3, &
        & h2o, co2, n2o, ch4, cfc11, cfc12, flux_up, flux_dn, flux_net
        REAL(Kind=8), DIMENSION(:, :) :: sfc_t, sfc_emis
        REAL(Kind=8) :: o2
        call lw_func(x, y, lay, lev, lay_p, lay_t, lev_p, lev_t, l_wp, i_wp, re_l, re_i, sfc_t, sfc_emis, &
                     & h2o, co2, o3, n2o, ch4, o2, cfc11, cfc12, flux_up, flux_dn, flux_net)
    END SUBROUTINE lw_rrtmgp
    
    SUBROUTINE lw_init()
        USE C_FUNC
        IMPLICIT NONE
        call init()
    END SUBROUTINE lw_init
    
END MODULE LWModule
